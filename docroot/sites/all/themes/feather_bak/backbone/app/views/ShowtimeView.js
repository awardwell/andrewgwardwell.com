define([
  'app/views/BaseView',
  'moment',
  'text!app/templates/showtime.html'
], function (BaseView, moment, template) {
  var ShowtimeView = BaseView.extend({
    tagName: 'li',
    template: template,
    id : function(){
      var movie = this.model.get('movie');
      return movie.movie_id;
    }
  });

  return ShowtimeView;
});