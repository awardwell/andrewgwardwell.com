define([
  'app/views/MovieOverView',
  'underscore',
  'utility',
  'moment',
  'text!app/templates/eventcinemaoverview.html'
], function (MovieOverView, _, u, moment, template) {
  var EventCinemaOverView = MovieOverView.extend({
    tagName: 'div',
    className: 'movie_overview',
    template: template,

    events: {
      'change #movie_filter_day': 'filter'
    },

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
      this.listenTo(this.collection, 'sync', this.render);
    },

    preprocess: function(data) {
      data.releases = _.union(['All Dates'], _.filter(_.uniq(_.map(_.sortBy(_.flatten(_.pluck(_.flatten(this.collection.pluck("showtimes")), "showtime")), function(item){
        return item;
      }), function(item){
        return moment(item, 'X').utc().format('ddd, MMM Do');
      })), function(item){
        return moment(item, 'ddd, MMM Do').utc() >= moment(moment().utc().format('ddd, MMM, Do'), 'ddd, MMM, Do').utc();
      }));
    },

    postprocess: function() {
      var filters = window.location.hash.replace('#filter/', '');
      var allTheThings = u.pathToObj(filters);
      this.$('#movie_filter_day option[value="' + allTheThings.date + '"]').prop('selected', true);
    },

    filter: function (e) {
      // Get the selected values for the filter (DOM elements)
      var searches = {
        date: $('#movie_filter_day').val(),
      };

      this.childView.collection.filter(function(item) {
        // Find the genre from within the collections
        var dates = item.get('showtimes');
        // The Genres and the Dates are in an array format
        if (searches.date == 'All Dates'){
          hasDate = true;
        } else {
          var hasDate =  _.filter(dates, function(date){
            var models_date = moment(date.showtime, 'X').utc().format('ddd, MMM Do');
            return (models_date == searches.date);
          });
          if (hasDate.length > 0) {hasDate = true;} else {hasDate = false;}
        }

        return hasDate;
      });

      // Trigger the filter even and pass the object to the
      this.trigger('filter', 'filter/' + u.objToPath(searches));
    }
  });

  return EventCinemaOverView;
});