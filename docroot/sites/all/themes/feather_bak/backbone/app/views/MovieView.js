define([
  'app/views/BaseView',
  'text!app/templates/movie.html'
], function (BaseView, template) {
  var MovieView = BaseView.extend({
    tagName: 'li',
    className: 'movie',
    id: this.id,
    template: template

  });

  return MovieView;
});