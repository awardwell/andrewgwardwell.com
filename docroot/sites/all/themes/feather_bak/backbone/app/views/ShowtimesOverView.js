define([
  'app/views/BaseView',
  'underscore',
  'utility',
  'moment',
  'text!app/templates/movieoverview.html'
], function (BaseView, _, u, moment, template) {
  var MovieOverView = BaseView.extend({
    tagName: 'div',
    className: 'showtimes_filter',
    template: template,
    collection_2: {},

    events: {
      'change #movie_filter_genre': 'filter',
      'change #movie_filter_day': 'filter',
      'change #movie_filter_rating': 'filter'
    },

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
    },

    preprocess: function(data) {
      data.ratings = _.filter(_.uniq(_.pluck(this.collection.pluck('movie'), 'rating')), function(item){
        return item;
      });

      data.genres = _.sortBy(_.map(_.filter(_.uniq(_.flatten(_.pluck(this.collection.pluck('movie'), 'genre'))), function(item){
        return _.contains(['Action-Adventure', 'Animation', 'Bollywood', 'Comedy', 'Documentary', 'Drama', 'Family', 'Program', 'Romance', 'SciFi-Fantasy', 'Suspense-Thriller'], item);
      }), function(item){
        return item.replace('-', '/').replace('Program', 'Event Cinema');
      }), function(item){
        return item;
      });

      data.releases = _.uniq(_.map(_.sortBy(_.flatten(_.pluck(_.flatten(this.collection.pluck('showtimes')), 'showtime')), function (item) {
        return item;
      }), function (item) {
        if (moment().format('ddd, MMM Do') == moment(item, 'X').utc().format('ddd, MMM Do')) {
          return 'Today';
        } else {
          return moment(item, 'X').utc().format('ddd, MMM Do');
        }
      }));
    },

    postprocess: function() {
      var filters = window.location.hash.replace('#filter/', '');
      var allTheThings = u.pathToObj(filters);
      this.$('#movie_filter_genre option[value="' + allTheThings.genre + '"]').prop('selected', true);
      this.$('#movie_filter_rating option[value="' + allTheThings.rating + '"]').prop('selected', true);
      this.$('#movie_filter_day option[value="' + allTheThings.date + '"]').prop('selected', true);
    },

    filter: function (e) {
      // Get the selected values for the filter (DOM elements)
      var searches = {
        genre: $('#movie_filter_genre').val().replace('/', '-').replace('Event Cinema', 'Program'),
        date: $('#movie_filter_day').val(),
        rating: $('#movie_filter_rating').val()
      };

      this.childView.collection.filter(function(item) {
        // Find the genre from within the collections
        var movie = item.get('movie'),
          genres = movie.genre,
          rating = movie.rating,
          dates = item.get('showtimes');
        // The Genres and the Dates are in an array format
        var hasGenre =  _.contains(genres, searches.genre);
        if (searches.date == 'Today'){
          searches.today = 'Today';
          searches.date = moment().format('ddd, MMM Do');
        }
        var hasDate = _.sortBy(_.filter(dates, function(date){
          var models_date = moment(date.showtime, 'X').utc().format('ddd, MMM Do');
          return (models_date == searches.date);
        }), function (item) {
          if (moment(item.showtime, 'X').utc().format('HH') == '00') {
            return moment(item.showtime, 'X').utc().add('days', 1).format('X');
          } else {
            return item.showtime;
          }
        });
        item.set('display_showtimes', hasDate);
        // The rating is a single value
        var hasRating = rating == searches.rating;
        // If our all option is selected for one of the elements
        if (searches.genre == 'All Genres') {hasGenre = true;}
        if (searches.rating == 'All Ratings') {hasRating = true;}
        if (hasDate.length > 0) {hasDate = true;} else {hasDate = false;}
        return hasGenre && hasRating && hasDate;
      });
      // Convert todays searches.date back into 'Today' if it has been set (for use in the URL)
      if(searches.today){
        searches.date = searches.today;
        delete searches.today;
      }
      // Trigger the filter even and pass the object to the
      this.trigger('filter', 'filter/' + u.objToPath(searches));
    }


  });

  return MovieOverView;
});