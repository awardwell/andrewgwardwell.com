
define([
  'backbone',
  'mustache',
  'utility',
  'underscore',
  'MapsUtility',
  'moment',
  'app/views/EventView',
  'text!app/templates/eventstabs.html'
], function (Backbone, mustache, u, _, map, moment, EventView, template) {
  var TheatreEventsView = Backbone.View.extend({
    id: 'theatre_events_list_wrapper',

    template: template,

    events: {
//      'click #theatre_events_month_tabs li' : 'filterMonth'
    },

    initialize: function () {
      // Get Location information
      this.listenTo(this.collection, 'reset', this.render);
    },

    preprocess : function (data){
      this.collection.models = this.collection.sortBy(function(item){
        return item.get('start_time');
      });
      var now = moment().date(15).format('MMM');
      var now_num = moment().date(15).format('M');
      data.months = [
        {
          number: now_num,
          letter: now
        },
        {
          number: moment().add('M', 1).format('M'),
          letter: moment().add('M', 1).format('MMM')
        },
        {
          number: moment().add('M', 2).format('M'),
          letter: moment().add('M', 2).format('MMM')
        }
      ];

    },

    render: function() {
      this.$el.html('');
      var data = {};
      // Get the collection ready to render
      this.preprocess(data);
      // boilerplate
      this.$el.html(mustache.render(this.template, data));
      this.collection.each(function(item) {
        var view = new EventView({
          model: item
        });
        this.$('.theatre_event_list').append(view.render().el);
      }, this);
      this.postprocess();
      return this;
    },

    postprocess : function(){
    // Get the first theater in the DOM and give it nearest class
      var raw_date = _.uniq(this.collection.pluck('start_time')),
          mon_id = moment(raw_date, 'X').utc().format('M');
      this.$('[data-month="'+mon_id+'"]').addClass('active');
    },

    filter: function(id){
      // Filter by theatre.
      this.collection.filter(function(item){
      var the_id = id,
          ids = item.get('house_ids'),
          start = moment(item.get('start_time'), 'X').utc().format('MMM'),
          now = moment().date(15).format('MMM'),
          has_id = _.contains(ids, the_id),
          current_month = now == start;
      return has_id && current_month;
      });
    },

    filterMonth: function(id){
      this.collection.filter(function(item){
        var the_id = $('.nearest').attr('id'),
          ids = item.get('theatre_ids'),
          start = moment(item.get('start_time'), 'X').utc().format('MMM'),
          comp_month = moment(id, 'M').format('MMM'),
//          now = moment().date(15).format('MMM'),
          has_id = _.contains(ids, the_id),
          current_month = comp_month == start;
        return has_id && current_month;
      });
    }

  });

  return TheatreEventsView;
});