define([
  'backbone',
  'mustache',
  'utility',
  'text!app/templates/map.html'
], function (Backbone, mustache, u, template) {
  var TheatreMapView = Backbone.View.extend({
    // Truthfully this is just a wrapper view for the sake of organization, and
    // potentially expnadability.

    template: template,
    render: function() {
      var data = {};
      this.$el.html(mustache.render(this.template, data));
      return this;
    }
  });

  return TheatreMapView;
});
