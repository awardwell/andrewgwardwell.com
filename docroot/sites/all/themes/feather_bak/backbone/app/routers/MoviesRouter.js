define([
  'backbone',
  'utility',
  'moment',
  'jquery'
],
function (Backbone, u, moment, $) {
  var MoviesRouter = Backbone.Router.extend({
    routes: {
      /*
        Filters can be of the following format:
        filter/genre/Comedy
        filter/rating/PG-13
        filter/genre/Comedy/rating/PG-13
        filter/rating/PG-13/genre/Comedy
       */
      '*generic': 'generic'
//      'filter/*filters': 'filter',
//      'highlight/*filters': 'highlight'
    },

    initialize: function(){
      this.on('route', this.trackPage);
    },

    trackPage: function(){
      var url = window.location.href + '/' + window.location.hash;
//      var titleObj = u.pathToObj();
//      ga('send', {
//        'hitType': 'pageview',
//        'page': url
//      });
    },

    filter: function (filters) {
      var filters = u.pathToObj(filters);

      // Give the filter values triggered by the route
      $('#movie_filter_genre option[value="' + filters.genre + '"]').prop('selected', true);
      $('#movie_filter_rating option[value="' + filters.rating + '"]').prop('selected', true);
      $('#movie_filter_day option[value="' + filters.date + '"]').prop('selected', true);

      // filter the collection
      this.collection.filter(function(item) {
        // Find the genre from within the collections
        var genres = item.get('genre'),
          rating = item.get('rating'),
          dates = item.get('showtimes');
        // The Genres and the Dates are in an array formats
        var hasGenre =  _.contains(genres, filters.genre);
        if (filters.date == 'Today'){
          filters.date = moment().format('ddd, MMM Do');
        }
        var hasDate = _.sortBy(_.filter(dates, function(date){
            var models_date = moment(date.showtime, 'X').utc().format('ddd, MMM Do');
            return (models_date == filters.date);
        }), function (item) {
          if (moment(item.showtime, 'X').utc().format('HH') == '00') {
            return moment(item.showtime, 'X').utc().add('days', 1).format('X');
          } else {
            return item.showtime;
          }
        });
        // The rating is a single value
        var hasRating = rating == filters.rating;
        // If our all option is selected for one of the elements
        item.set('display_showtimes', hasDate);
        if (hasDate.length > 0 || typeof filters.release == 'undefinded') {hasDate = true;} else {hasDate = false;}
        if (filters.genre == 'All Genres' || typeof filters.genre == 'undefined') {hasGenre = true;}
        if (filters.rating == 'All Ratings' || typeof filters.rating == 'undefined') {hasRating = true;}
        return hasGenre && hasRating && hasDate;
      });
    },

    highlight : function(filters){
      var obj = u.pathToObj(filters);
//     @todo make a revers filter -- by release date so if the movie is not available Today we go to the first day it's available
//      1) pull movie data from the collection
//      2) Sort showtimes by timestamp
//      3) Get the earliest one and filter date to that day. BOOM
      // returns the model (first result)
        var movie = _.find(Drupal.settings.showMovies, function(item){
            return item.movie.movie_id === obj.movie_id;
        }),
          earliest;
//        if (moment(movie.showtimes[0].showtime, 'X').utc().format('HH') == '00') {
//          earliest = moment(_.min(_.pluck(movie.showtimes, 'showtime')), 'X').utc().subtract('days', 1).format('ddd, MMM Do');
//        } else {
          earliest = moment(_.min(_.pluck(movie.showtimes, 'showtime')), 'X').utc().format('ddd, MMM Do');
//        }
        $('#movie_filter_day option[value="' + earliest + '"]').prop('selected', true);
      this.collection.filter(function(item) {
        // Find the showtimes from the movie
        var dates = item.get('showtimes');
        // In the list of showtimes does this movie have a showtime that matches the earliest for this movie?
        var datePresent =  _.some(dates, function(ind){
//          if (moment(movie.showtimes[0].showtime, 'X').utc().format('HH') == '00') {
//            var models_date = moment(ind.showtime, 'X').utc().subtract('hours', 2).format('ddd, MMM Do');
//          } else {
            var models_date = moment(ind.showtime, 'X').utc().format('ddd, MMM Do');
//          }
          return models_date == earliest;
        });
        var hasDate = _.sortBy(_.filter(dates, function(ind_1){
          var models_date_1 = moment(ind_1.showtime, 'X').utc().format('ddd, MMM Do');
          return (models_date_1 == earliest);
        }), function (item) {
          if (moment(item.showtime, 'X').utc().format('HH') == '00') {
            return moment(item.showtime, 'X').utc().add('days', 1).format('X');
          } else {
            return item.showtime;
          }
        });
        // The rating is a single value
        var hasRating = true;
        var hasGenre = true;
        item.set('display_showtimes', hasDate);
        return hasGenre && hasRating && datePresent;
      });

      if ($('#showtime_list').length > 0) {
        var $el = $('#'+obj.movie_id);
        if ($el.length > 0) {
          var p = $el.position();
          $(document).scrollTop(p.top);
          $el.addClass('active');
        }
      }
    },

    generic: function (filters) {
      if (filters != null) {
        var split = filters.split('|');
        var that = this;
        _.each(split, function (item) {
          var splitArray = item.split('/');
          var type = splitArray[0];
          splitArray.shift();
          var path = splitArray.join('/');
          switch(type) {
            case 'filter' :
              that.filter(path);
              break;
            case 'highlight' :
              that.highlight(path);
              break;
          }
        });
      }
    }
  });

  return MoviesRouter;
});